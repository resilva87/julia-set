#include <complex.h>
#include <math.h>
#include "computation.h"

int Iterations_Before_Bail(int maxIterations, double complex pixelPoint, double complex initialPoint) {
	int iterations = 0;
	double complex oldPoint;
	for (; iterations < maxIterations; iterations++) {
		oldPoint = pixelPoint;
		double realNew = pow(creal(oldPoint), 2.0) - pow(cimag(oldPoint), 2.0) + creal(initialPoint);
		double imagNew = 2 * creal(oldPoint) * cimag(oldPoint) + cimag(initialPoint);
		pixelPoint = realNew + imagNew * I;
		if ((pow(realNew, 2.0) + pow(imagNew, 2.0)) > 4) 
			break;
	}
	return iterations;
}
