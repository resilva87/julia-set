#ifndef COLORS_H
#define COLORS_H

#include <SDL2/SDL.h>

typedef struct hsv
{
	double h; // 0 - 360
	double s; // 0 - 255
	double v; // 0 - 255
} HSV;

typedef struct rgb{
	Uint8 r; // 0 - 255
	Uint8 g; // 0 - 255
	Uint8 b; // 0 - 255
} RGB;

void createHSV(HSV *hsv);
void hsv2rgb(RGB *p, HSV hsv);

#endif
