#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include "computation.h"
#include "colors.h"
#include "colormap.h"
#include "julia.h"

// SDL objects
SDL_Window *window = NULL;

SDL_Renderer *renderer = NULL;

// The surface contained by the window
SDL_Surface* mainSurface = NULL;

colorMap1D *colorMap = NULL;

double initialReal;
double initialImag;

double complex initialPoint;

// Screen attributes
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

const int MAX_ITERATIONS = 300;

bool init () {
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		return false;
	}
	window = SDL_CreateWindow("JuliaSet", 
										  SDL_WINDOWPOS_CENTERED, 
										  SDL_WINDOWPOS_CENTERED, 
	 					  				  SCREEN_WIDTH, SCREEN_HEIGHT, 
	 					  				  SDL_WINDOW_SHOWN);
	if (window == NULL) {
		printf("Could not create window\n");
		printf("%s\n", SDL_GetError());
		cleanup();
		return false;
	}
	renderer = SDL_CreateRenderer(window, -1, 
							 SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL) {
		printf("Could not create renderer\n");
		printf("%s\n", SDL_GetError());
		cleanup();
		return false;
	}
	mainSurface = SDL_GetWindowSurface(window);
	srand(time(NULL));
	// random fractal spec
	//initialReal = -1+2*((double)rand())/(double)RAND_MAX;
	//sleep(1);
	//initialImag = -1+2*((double)rand())/(double)RAND_MAX;
	//initialPoint = initialReal + initialImag * I;
	return true;
}

bool initMap() {
	SDL_Color *colors = (SDL_Color *) malloc(sizeof(SDL_Color) * 3);
	colors[0] = ACQUA;
	colors[1] = YELLOW;
	colors[2] = TEAL;
	// colors[3] = BLUE;
	// colors[4] = PURPLE;
	colorMap = initColorMap(STEPS, colors, 3);
	return true;
}

void cleanup () {
	if (window) {
		SDL_DestroyWindow(window);
	}
	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	destroyColorMap(colorMap);
	SDL_Quit();
}

void Draw_Pixel(int x, int y, SDL_Color color) {
	if(x < 0 || y < 0 || x >= SCREEN_WIDTH || y >= SCREEN_HEIGHT) return;
	Uint32 colorSDL = SDL_MapRGB(mainSurface->format, color.r, color.g, color.b);
	Uint32* bufp = (Uint32*)mainSurface->pixels + y * mainSurface->pitch / 4 + x;
	*bufp = colorSDL;
}

int main(int argc, char* argv[]){
	if(argc != 3){
		printf("Expecting two parameters: julia.out <double>real <double>imag\n");
		exit(-1);
	}
	initialReal = strtod(argv[1], NULL);
	initialImag = strtod(argv[2], NULL);
	initialPoint = initialReal + initialImag * I;
	bool done = false;
	SDL_Event e;	
	if (!init() || !initMap()) {
		SDL_Quit();
		return 1;
	}
	
	SDL_FillRect(mainSurface, NULL, SDL_MapRGB(mainSurface->format, 0x00, 0x00, 0x00));
	SDL_UpdateWindowSurface(window);
	for (int y = 0; y < SCREEN_HEIGHT; y++) {
		for (int x = 0; x < SCREEN_WIDTH; x++) {
			double pointReal = 1.5 * (x - SCREEN_WIDTH / 2) / (0.5 * SCREEN_WIDTH);
			double pointImag = (y - SCREEN_HEIGHT / 2) / (0.5 * SCREEN_HEIGHT);
			double complex pixelPoint = pointReal + pointImag * I;
			int iterations  = Iterations_Before_Bail(MAX_ITERATIONS, pixelPoint, initialPoint);
			double r = (1.0 * iterations / MAX_ITERATIONS);
			SDL_Color* color = getColorFromMap(colorMap, r);
			Draw_Pixel(x, y, *color);
		}
	}
	SDL_UpdateWindowSurface(window);
	printf("done\n");
	while (!done) {
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT) {
				done = true;
			}
		}
		// Update the surface
		SDL_UpdateWindowSurface(window);
		SDL_Delay(5);
	}
	cleanup();
	return 0;
}