CC=gcc
CFLAGS=-std=c99 -Wall -LLIBDIR -Wl,-rpath -Wl,/usr/local/lib -g
LDFLAGS=-lSDL2 -lm
SOURCES=julia.h julia.c computation.h computation.c colors.h colors.c colormap.h colormap.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=julia.out

all: 
	$(CC) $(CFLAGS) -o $(EXECUTABLE) $(SOURCES) $(LDFLAGS)

clean:
	rm $(EXECUTABLE)