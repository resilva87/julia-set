#ifndef COLORMAP_H
#define COLORMAP_H

#include <SDL2/SDL.h>

#define STEPS 1024

// Main colors
// http://www.december.com/html/spec/color16codes.html
#define WHITE (SDL_Color) {.r = 255, .g = 255, .b = 255};
#define BLACK (SDL_Color) {.r = 0, .g = 0, .b = 0};
#define GRAY (SDL_Color) {.r = 128, .g = 128, .b = 128};
#define SILVER (SDL_Color) {.r = 192, .g = 192, .b = 192};
#define RED (SDL_Color) {.r = 255, .g = 0, .b = 0};
#define MAROON (SDL_Color) {.r = 128, .g = 0, .b = 0};
#define OLIVE (SDL_Color) {.r = 128, .g = 128, .b = 0};
#define YELLOW (SDL_Color) {.r = 255, .g = 255, .b = 0};
#define GREEN (SDL_Color) {.r = 0, .g = 128, .b = 0};
#define LIME (SDL_Color) {.r = 0, .g = 255, .b = 0};
#define TEAL (SDL_Color) {.r = 0, .g = 128, .b = 128};
#define ACQUA (SDL_Color) {.r = 0, .g = 255, .b = 255};
#define NAVY (SDL_Color) {.r = 0, .g = 0, .b = 128};
#define BLUE (SDL_Color) {.r = 0, .g = 0, .b = 255};
#define PURPLE (SDL_Color) {.r = 128, .g = 0, .b = 128};
#define FUSCHIA (SDL_Color) {.r = 255, .g = 0, .b = 255};

typedef struct colorMap1D_n colorMap1D;

struct colorMap1D_n {
	int length;
	SDL_Color **p;
};

colorMap1D *initColorMap(int steps, SDL_Color *colors, int colorsLength);

SDL_Color *getColorFromMap(colorMap1D *map, double value); 

void destroyColorMap(colorMap1D *map);

#endif