# julia-set
Rendering Julia sets in C with SDL2 :smiley_cat:

## Samples

![sample-1](https://github.com/resilva87/julia-set/raw/master/img/sample1.png)
![sample-2](https://github.com/resilva87/julia-set/raw/master/img/sample2.png)
![sample-3](https://github.com/resilva87/julia-set/raw/master/img/sample3.png "This one is crazy")

## Running
You'll need libSDL2 installed in your system (currently building as shared lib).

*** Install freeglut3-dev or similar for OpenGL support on your system ***
	
    git clone this
    make
    ./julia.out <double>real <double>imag

## Applying Color Map

Following [this great post](http://stackoverflow.com/questions/23711681/generating-custom-color-palette-for-julia-set) the app is almost fully enabled to use a color map (the  pictures above are rendered using ./julia.out -0.74123123 0.1289787).

![inter-1](https://github.com/resilva87/julia-set/raw/master/img/after-color-mapping/sample1.png "5-collor interpolation: red, yellow, green, blue, purple")
![inter-2](https://github.com/resilva87/julia-set/raw/master/img/after-color-mapping/sample2.png "3-collor interpolation: black, yellow, olive")


## TODO

* ~~Select RGB color randomly and from that point on compute HSV values~~ (actually this was in turn made as a color mapping scheme)
* Toolchain (configure, make, gradle?)
* Release
* Smooth color interpolation from map
* Sine interpolation (more general function application to the value)
* Choose colors from command line
* As screensaver :bangbang: