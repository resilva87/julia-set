#include <math.h>
#include "colors.h"

void hsv2rgb(RGB *p, HSV hsv) {
  // this is from http://lodev.org/cgtutor/juliamandelbrot.html
  float r, g, b, h, s, v; //this function works with floats between 0 and 1
  h = hsv.h;
  s = hsv.s / 256.0;
  v = hsv.v / 256.0;

  //if saturation is 0, the color is a shade of grey
  if(s == 0.0) r = g = b = v;
//if saturation > 0, more complex calculations are needed
  else
  {
    float f, p, q, t;
    int i = ((int)floor((int)(h / 60))) % 6;
    f = h - i;//the fractional part of h

    p = v * (1.0 - s);
    q = v * (1.0 - (s * f));
    t = v * (1.0 - (s * (1.0 - f)));

    switch(i)
    {
      case 0: r=v; g=t; b=p; break;
      case 1: r=q; g=v; b=p; break;
      case 2: r=p; g=v; b=t; break;
      case 3: r=p; g=q; b=v; break;
      case 4: r=t; g=p; b=v; break;
      case 5: r=v; g=p; b=q; break;
      default: r = g = b = 0; break;
    }
  }
  
  p->r = (int)(r * 255.0);
  p->g = (int)(g * 255.0);
  p->b = (int)(b * 255.0);
}
