#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "colormap.h"

// Mostly implemented as a reference from this great post
// http://stackoverflow.com/questions/23711681/generating-custom-color-palette-for-julia-set

void fillColorMap(colorMap1D *map, SDL_Color *color) {
	for(int i = 0; i < map->length; i++){
		map->p[i] = color;
	}
}

void fillColor(colorMap1D *map, int pos, SDL_Color color0, SDL_Color color1, double localRel) {
	int dr = color1.r - color0.r;
	int dg = color1.g - color0.g;
	int db = color1.b - color0.b;
	SDL_Color *newRGB = (SDL_Color *) malloc(sizeof(SDL_Color));
	newRGB->r = (Uint8) (color0.r + localRel * dr);
	newRGB->g = (Uint8) (color0.g + localRel * dg);
	newRGB->b = (Uint8) (color0.b + localRel * db);
	map->p[pos] = newRGB;
	// printf("ok %d\n", pos);
}

colorMap1D *initColorMap(int steps, SDL_Color *colors, int colorsLength) {
	colorMap1D *map = (colorMap1D *) malloc(sizeof(colorMap1D));
	map->length = steps;
	map->p = (SDL_Color **) malloc(sizeof(SDL_Color*) * steps);
	if(colorsLength == 1){
		fillColorMap(map, colors);
		return map;
	}
	double colorDelta = 1.0 / (colorsLength - 1);
	for (int i=0; i<steps; i++) {
		double globalRel = (double)i / (steps - 1);
		int index0 = (int)(globalRel / colorDelta);
        int index1 = (int)fmin(1.0 * colorsLength-1, 1.0 * index0 + 1);
        double localRel = (globalRel - index0 * colorDelta) / colorDelta;
        // printf("index0:%d index1: %d localRel: %f\n", index0, index1, localRel);
        SDL_Color color0 = colors[index0];
        SDL_Color color1 = colors[index1];
        // printf("color0: (%d, %d, %d) color1: (%d, %d, %d)\n", color0.r, color0.g, color0.b, color1.r, color1.g, color1.b);
        fillColor(map, i, color0, color1, localRel);
	}
	return map;
}

SDL_Color *getColorFromMap(colorMap1D *map, double value) {
	double x  = fmax(0.0, fmin(1.0, value));
	int pos = (int)(x * (map->length - 1));
	if(pos < 0 || pos > map->length-1){
		return &BLACK;
	}
	return map->p[pos];
 }

void destroyColorMap(colorMap1D *map) {
	if(map == NULL)
		return;
	for(int i = 0; i < map->length; i++) {
		free(map->p[i]);
	}
	free(map);
}