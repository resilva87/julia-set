#ifndef COMPUTATION_H
#define COMPUTATION_H
#include <complex.h>

int Iterations_Before_Bail(int maxIterations, double complex pixelPoint, double complex initialPoint);

#endif