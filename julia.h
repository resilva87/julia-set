#ifndef JULIA_H
#define JULIA_H
#include <stdbool.h>
#include <SDL2/SDL.h>
#include "colors.h"

void Draw_Pixel(int x, int y, SDL_Color color);
void cleanup();
bool init();
bool initMap();

#endif